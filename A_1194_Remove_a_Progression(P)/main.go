package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() 
	scanner.Text()
	for scanner.Scan() {
		text := scanner.Text()
		num := strings.Split(text, " ")
		x, _ := strconv.Atoi(num[1])
		fmt.Println(x + x)
	}
}
