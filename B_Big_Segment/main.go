package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	inputs := make([]string, 0, 100000)
	for {
		scanner.Scan()
		text := scanner.Text()
		if text == "" {
			break
		}
		inputs = append(inputs, text)
	}
	no, _ := strconv.Atoi(inputs[0])
	segments := splitSeg(inputs[1:])
	if no != len(segments) {
		panic("Wrong inputs")
	}
	res := 0
	min, max := segments[res][0], segments[res][1]
	for i, seg := range segments {
		if seg[0] <= min && seg[1] >= max {
			min = seg[0]
			max = seg[1]
			res = i + 1
		} else {
			if seg[0] < min {
				min = seg[0]
				res = -1
			}
			if seg[1] > max {
				max = seg[1]
				res = -1
			}
		}
	}
	fmt.Println(res)
}

func splitSeg(segs []string) [][]int {
	res := make([][]int, 0, len(segs))
	for _, s := range segs {
		v := strings.Split(s, " ")
		v1, _ := strconv.Atoi(v[0])
		v2, _ := strconv.Atoi(v[1])
		res = append(res, []int{v1, v2})
	}

	return res
}
