package main

import (
	"flag"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	i1 := flag.Int("i1", 0, "Input interger n")
	i2 := flag.String("i2", "", "Input n integers")
	flag.Parse()
	no := *i1
	times := stringsToInts(strings.Split(*i2, ","))
	if len(times) != no {
		panic("Wrong input")
	}
	total := 0
	start := 0
	if times[len(times)-1] < 91 {
		times = append(times, 90)
	}
	for _, t := range times {
		count := 0
		for i := start; i < t; i++ {
			count++
			if count > 15 {
				fmt.Println(total)
				return
			}
			total++
		}
		start = t
	}
	fmt.Println(total)
}

func stringsToInts(s []string) []int {
	res := make([]int, 0, len(s))
	for _, v := range s {
		i, _ := strconv.Atoi(v)
		res = append(res, i)
	}

	return res
}
