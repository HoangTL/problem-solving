package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	inputs := make([]string, 0, 100000)
	for {
		scanner.Scan()
		text := scanner.Text()
		if text == "" {
			break
		}
		inputs = append(inputs, text)
	}
	no, _ := strconv.Atoi(inputs[0])
	input2 := inputs[1:]
	if no != len(input2) {
		panic("Wrong inputs")
	}
	safePos := no / 2
	var res1, res2 string
	for i := 0; i < safePos; i++ {
		res1 += "1"
		res2 += "1"
	}
	convInputs := splitInput(input2)
	for i := safePos; i < len(convInputs); i++ {
		if convInputs[i][0] < convInputs[i-(i-safePos+1)][1] {
			res1 += "1"
			res2 += "0"
		} else if convInputs[i][1] < convInputs[i-(i-safePos+1)][0] {
			res2 += "1"
			res1 += "0"
		} else {
			res1 += "0"
			res2 += "0"
		}
	}

	fmt.Println(res1)
	fmt.Println(res2)
}

func splitInput(inputs []string) [][]int {
	res := make([][]int, 0, len(inputs))
	for _, v := range inputs {
		split := strings.Split(v, " ")
		v1, _ := strconv.Atoi(split[0])
		v2, _ := strconv.Atoi(split[1])
		res = append(res, []int{v1, v2})
	}

	return res
}
