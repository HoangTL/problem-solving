package main

import (
	"bufio"
	"os"
)

var (
	upper = byte("AZ")
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanLines)
	specChar := []byte(";_-")
	sChar := map[byte]struct{}{
		specChar[0]: struct{}{},
		specChar[1]: struct{}{},
		specChar[2]: struct{}{},
	}
	name := make(map[byte][]byte)

	for i := 0; i < 3; i++ {
		scanner.Scan()
		text := scanner.Bytes()
		newText := make([]byte, 0, len(text))
		for _, b := range text {
			if _, ok := sChar[c]; !ok {
				newText = append(newText, c)
			}
		}
		name = append(name, newText)
	}
}

func lowerCase(b byte) byte {
	if b > upper[0] && b < upper[1] {
		b = b - 32
	}

	return b
}
