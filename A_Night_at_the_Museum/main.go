package main

import (
	"flag"
	"fmt"
	"math"
)

func main() {
	input := flag.String("i", "", "Input name")
	flag.Parse()
	name := []byte(*input)
	alphabet := []byte("a")
	start := int(alphabet[0])
	count := 0
	for _, s := range name {
		v := int(s)
		left := int(math.Abs(float64(v - start)))
		right := 26 - left
		if left >= right {
			count += int(right)
		} else {
			count += int(left)
		}
		start = v
	}
	fmt.Println(count)
}
