package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() 
	text := scanner.Text()
	num := strings.Split(text, " ")
	move, _ := strconv.Atoi(num[0])
	candies, _ := strconv.Atoi(num[1])
	eat := 0
	addCount := 0
	added := 0

	for i := 1; i <= move; i++ {
		if added <= candies {
			addCount++
			added += addCount
		} else {
			added--
			eat++
		}
	}

	fmt.Println(eat)
}
