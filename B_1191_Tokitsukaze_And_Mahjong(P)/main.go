package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() 
	koutsu := make(map[string]int)
	shuntsu := make(map[string][]int)
	text := scanner.Text()
	tiles := strings.Split(text, " ")
	for _, s := range tiles {
		if _, ok := koutsu[s]; !ok {
			koutsu[s] = 1
		} else {
			koutsu[s]++
		}
		char := strings.Split(s, "")
		suit := char[1]
		digit, _ := strconv.Atoi(char[0])
		digit = digit - 1
		if _, ok := shuntsu[suit]; !ok {
			arr := make([]int, 9)
			arr[digit] = 1
			shuntsu[suit] = arr
		} else {
			shuntsu[suit][digit] = 1
		}
	}

	minKou := 2
	for _, v := range koutsu {
		if 3 - v < minKou {
			minKou = 3 - v
		}
		if minKou == 0 {
			fmt.Println(minKou)
			return
		}
	}
	minShun := 2
	for _, arr := range shuntsu {
		count := 0
		for i := 0; i < len(arr) - 2; i++ {
			count = arr[i] + arr[i + 1] + arr[i + 2]
			if 3 - count < minShun {
				minShun = 3 - count
			}
		}
		if minShun == 0 {
			fmt.Println(minShun)
			return
		}
	}
	if minKou < minShun {
		fmt.Println(minKou)
	} else {
		fmt.Println(minShun)
	}
}
