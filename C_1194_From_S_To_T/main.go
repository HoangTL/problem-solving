package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Input struct {
	s string
	t string
	p string
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	num, _ := strconv.Atoi(scanner.Text())
	arr := make([]string, 0, num*3)
	for scanner.Scan() {
		text := scanner.Text()
		arr = append(arr, text)
	}
	for i := 0; i < len(arr); i += 3 {
		input := Input{
			s: arr[i],
			t: arr[i+1],
			p: arr[i+2],
		}
		calculate(input)
	}
}

func calculate(input Input) {
	missing := make(map[rune]int)
	if len(input.s) > len(input.t) {
		fmt.Println("NO")
		return
	}
	for _, r := range input.t {
		if _, ok := missing[r]; !ok {
			missing[r] = 1
		} else {
			missing[r]++
		}
	}
	i := 0
	valid := true
	for _, r := range input.s {
		if _, ok := missing[r]; ok {
			missing[r]--
		} else {
			fmt.Println("NO")
			return
		}
		for ; i < len(input.t); i++ {
			if string(input.t[i]) == string(r) {
				if i + 1 < len(input.t) {
					i++
				}
				break
			}
		} 
		if i == len(input.t) {
			valid = false
		}
	}
	if !valid {
		fmt.Println("NO")
		return
	}
	for _, r := range input.p {
		if _, ok := missing[r]; ok {
			missing[r]--
		}
	}
	for _, v := range missing {
		if v > 0 {
			fmt.Println("NO")
			return
		}
	}
	fmt.Println("YES")
}
