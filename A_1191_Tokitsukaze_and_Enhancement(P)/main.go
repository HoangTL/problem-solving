package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	A = 1
	B = 3
	C = 2
	D = 0
)
func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() 
	text := scanner.Text()
	num, _ := strconv.Atoi(text)
	switch num % 4 {
	case A:
		fmt.Println("0 A")
	case B:
		fmt.Println("2 A")
	case C:
		fmt.Println("1 B")
	case D:
		fmt.Println("1 A")
	default:
		fmt.Println("0 A")
	}
}
