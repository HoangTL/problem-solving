package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	scanner.Text()
	r, _ := regexp.Compile(`R\d+C\d+`)
	for scanner.Scan() {
		text := scanner.Text()
		notExcel := r.MatchString(text)
		if notExcel {
			convertExcelLikeSystem(text)
		} else {
			convertOtherSystem(text)
		}
	}
}

func convertOtherSystem(text string) {
	wr, _ := regexp.Compile(`\D+`)
	dr, _ := regexp.Compile(`\d+`)
	col := wr.FindAllString(text, -1)[0]
	row := dr.FindAllString(text, -1)[0]
	colNo := 0
	size := len(col) - 1
	for i, r := range col {
		dec := int(r) - 64
		colNo += dec * int(math.Pow(26, float64(size-i)))
	}

	fmt.Printf("R%sC%d\n", row, colNo)
}

func convertExcelLikeSystem(text string) {
	dr, _ := regexp.Compile(`\d+`)
	num := dr.FindAllString(text, -1)
	col, _ := strconv.Atoi(num[1])
	arr := make([]int, 0, 100)
	divide(col, &arr)
	str := ""
	for i, v := range arr {
		if v < 1 && i < len(arr) -1 {
			v = 27 - 1 + v
			arr[i+1]--
		} 
		if v > 0 {
			str = string(v + 64) + str
		}
	}
	fmt.Printf("%s%s\n", str, num[0])
}

func divide(num int, arr *[]int){
	res := num/26
	mod := num % 26
	*arr = append(*arr, mod)
	if res > 0 {
		divide(res, arr)
	}
}
