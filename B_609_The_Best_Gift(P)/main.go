package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var bookNo, genreNo int
	fmt.Scan(&bookNo)
	fmt.Scan(&genreNo)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	books := make([]int, genreNo)
	for scanner.Scan() {
		text := scanner.Text()
		i, _ := strconv.Atoi(text)
		books[i-1]++
	}

	var way int64
	for i := 0; i < genreNo; i++ {
		for j := i + 1; j < genreNo; j++ {
			way += int64(books[i]) * int64(books[j])
		}
	}

	fmt.Println(way)
}
