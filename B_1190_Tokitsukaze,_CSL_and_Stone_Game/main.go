package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	scanner.Scan()
	num, _ := strconv.Atoi(scanner.Text())
	sum1 := num * (num - 1) / 2
	arr := make([]int, 0, num)
	for scanner.Scan() {
		e, _ := strconv.Atoi(scanner.Text())
		arr = append(arr, e)
	}
	sum2 := 0
	count := make(map[int]int)
	for _, v := range arr {
		sum2 += v
		if _, ok := count[v]; ok {
			count[v]++
		} else {
			count[v] = 1
		}
	}
	count1 := 0
	for k, v := range count {
		if v > 2 {
			fmt.Println("cslnb")
			return
		} else if v == 2 {
			if k == 0 {
				fmt.Println("cslnb")
				return
			}
			if v2, ok := count[k-1]; ok && v2 == v-1 {
				fmt.Println("cslnb")
				return
			}
			count1++
		}
	}
	if count1 > 1 {
		fmt.Println("cslnb")
		return
	}
	res := sum2 - sum1
	if res%2 == 0 {
		fmt.Println("cslnb")
	} else {
		fmt.Println("sjfnb")
	}
}
