package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan() 
	scanner.Text()
	for scanner.Scan() {
		text := scanner.Text()
		num := strings.Split(text, " ")
		a, _ := strconv.Atoi(num[0])
		b, _ := strconv.Atoi(num[1])
		if a < b {
			a *= 2
		} else {
			b *= 2
		}
		if a > b {
			b = a
		} else {
			a = b
		}

		fmt.Println(a * b)
	}
}
