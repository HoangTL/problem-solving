package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	scanner.Scan()
	total, _ := strconv.Atoi(scanner.Text())
	uniq := make(map[int]int)
	arr := make([]int, 0, total)
	for scanner.Scan() {
		i, _ := strconv.Atoi(scanner.Text())
		if _, ok := uniq[i]; ok {
			uniq[i]++
		} else {
			uniq[i] = 1
		}
	}
	for k, v := range uniq {
		if v == 1 {
			delete(uniq, k)
		}
	}
	upper, lower := 0, 0
	for i, v := range arr {
		if _, ok := uniq[v]; ok {
			lower = i
			break
		}
	}
	for i := total; i > 0; i-- {
		if _, ok := uniq[arr[i]]; ok {
			upper = i
			break
		}
	}
	arr = arr[lower:upper]
	count := 0
	fmt.Println(count)
}
