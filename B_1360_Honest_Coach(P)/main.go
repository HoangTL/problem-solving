package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	cases, _ := strconv.Atoi(scanner.Text())
	for i := 0; i < cases; i++ {
		scanner.Scan()
		text := scanner.Text()
		l, _ := strconv.Atoi(text)
		scanner.Scan()
		strength := scanner.Text()
		arr := strings.Split(strength, " ")
		arr_i := make([]int, 0, l)
		for i := 0; i < l; i++ {
			v, _ := strconv.Atoi(arr[i])
			arr_i = append(arr_i, v)
		}
		sort.Ints(arr_i)
		res := -1
		for i := 0; i < l-1; i++ {
			diff := int(math.Abs(float64(arr_i[i] - arr_i[i+1])))
			if res == -1 {
				res = diff
			} else if diff < res {
				res = diff
			}
			if res == 0 {
				break
			}
		}
		fmt.Println(res)
	}
}
