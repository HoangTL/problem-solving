package main

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	inputs := make(chan string, 10000)
	w := &sync.WaitGroup{}

	scanner.Scan()
	lenText := scanner.Text()
	scanner.Scan()
	letters := scanner.Text()
	scanner.Scan()
	friend_no := scanner.Text()
	if friend_no == "" {
		fmt.Println(lenText)
		return
	}

	indexedLetters := indexString(&letters)
	w.Add(1)
	go calcuate(inputs, indexedLetters, w)
	for scanner.Scan() {
		text := scanner.Text()
		inputs <- text
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
	close(inputs)
	w.Wait()
}

func indexString(letters *string) map[rune][]int {
	res := make(map[rune][]int)
	for pos, r := range *letters {
		if _, ok := res[r]; ok {
			res[r] = append(res[r], pos + 1)
		} else {
			res[r] = []int{pos + 1}
		}
	}

	return res
}

func calcuate(ch chan string, letters map[rune][]int, w *sync.WaitGroup) {
	for s := range ch {
		letterCount := make(map[rune]int)
		max := 0
		for _, r := range s {
			if _, ok := letterCount[r]; ok {
				letterCount[r] += 1
			} else {
				letterCount[r] = 0
			}
		}
		for k, v := range letterCount {
			arr := letters[k]
			if arr[v] > max {
				max = arr[v]
			}
		}
		fmt.Println(max)
	}
	w.Done()
}
