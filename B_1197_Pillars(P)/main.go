package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	scanner.Scan()
	total, _ := strconv.Atoi(scanner.Text())
	arr := make([]int, 0, total)
	for scanner.Scan() {
		e, _ := strconv.Atoi(scanner.Text())
		arr = append(arr, e)
	}
	max := 0
	i := 0
	for ; i < len(arr); i++ {
		v := arr[i]
		if v > max {
			max = v
		} else {
			break
		}
	}
	for ; i < len(arr); i++ {
		v1 := arr[i-1]
		v2 := arr[i]
		if v2 > v1 {
			fmt.Println("NO")
			return
		}
	}
	fmt.Println("YES")
}
